package main

import (
	"catalog/handlers"
	"catalog/models"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/streadway/amqp"
)

func main() {

	host := os.Getenv("DB_HOST")
	rabbitMQURL := os.Getenv("AMQP_URL")
	dbUser := os.Getenv("DB_USER")
	dbName := os.Getenv("DB_NAME")
	dbPassword := os.Getenv("DB_PASSWORD")

	conn, err := amqp.Dial(rabbitMQURL)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	models.InitDB(dbUser, dbPassword, dbName, host)
	models.Migrate()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"catalog", // nombre de la cola
		true,      // durable
		false,     // eliminar cuando no esté en uso
		false,     // exclusivo
		false,     // no espera a que la cola se declare
		nil,       // argumentos adicionales
	)
	if err != nil {
		log.Fatal(err)
	}

	msgs, err := ch.Consume(
		q.Name, // cola
		"",     // consumidor
		true,   // auto-ack
		false,  // exclusivo
		false,  // no local
		false,  // no espera a que el consumidor esté listo
		nil,    // argumentos adicionales
	)
	if err != nil {
		log.Fatal(err)
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {

			// Decodificar el cuerpo del mensaje
			var request map[string]interface{}
			if err := json.Unmarshal(d.Body, &request); err != nil {
				log.Println("Error al decodificar el cuerpo del mensaje:", err)
				continue
			}

			// Simular algún procesamiento basado en el patrón
			var result interface{}
			switch pattern := request["pattern"].(string); pattern {
			case "GET_PRODUCT":
				productID := request["data"].(string)
				result, _ = handlers.GetProduct(productID)
			case "GET_CATALOG":
				result, _ = handlers.ListProducts()
			case "ADD_PRODUCT", "DELETE_PRODUCT":
				// Pasar la solicitud como parámetro
				result, _ = handlers.HandleProductRequest(request)
			default:
				result = "Operación no soportada"
			}

			// Estructura de la respuesta
			responseData := result

			// Codificación del cuerpo de la respuesta
			responseBody, err := json.Marshal(responseData)
			if err != nil {
				log.Println("Error al codificar la respuesta:", err)
				continue
			}

			// Publicar la respuesta en la cola de respuesta
			err = ch.Publish(
				"",        // intercambio
				d.ReplyTo, // cola de respuesta
				false,     // mandar sin confirmación
				false,     // no mandar como mandato
				amqp.Publishing{
					ContentType:   "application/json",
					CorrelationId: d.CorrelationId,
					Body:          responseBody,
				})
			if err != nil {
				log.Println("Error al publicar la respuesta:", err)
			}
		}
	}()
	fmt.Println(" [x] Esperando solicitudes RPC. Para salir, presiona CTRL+C")
	<-forever
}
func publishToRabbitMQ(conn *amqp.Connection, message string) {
	ch, err := conn.Channel()
	if err != nil {
		log.Println("Error al crear el canal RabbitMQ:", err)
		return
	}
	defer ch.Close()

	queue, err := ch.QueueDeclare(
		"catalog-logs",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Println("Error al declarar la cola RabbitMQ:", err)
		return
	}

	err = ch.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		},
	)
	if err != nil {
		log.Println("Error al publicar en RabbitMQ:", err)
		return
	}

	fmt.Printf("Mensaje publicado en RabbitMQ: %s\n", message)
}
