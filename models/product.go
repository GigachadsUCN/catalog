package models

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Product struct {
	gorm.Model
	Name        string
	Description *string `gorm:"type:text;default:'Sin descripcion'"`
	Stock       int
	Price       float64
	Category    string
	Color       string
	Genre       bool    //True Mujer False Hombre
	Image       *string `gorm:"type:text;default:'https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e_400x400.jpg'"`
}

var DB *gorm.DB

func Migrate() {
	DB.AutoMigrate(&Product{})
}

func InitDB(user, password, dbname, dbhost string) {
	var err error
	if user == "" {
		user = os.Getenv("DB_USER")
	}
	if password == "" {
		password = os.Getenv("DB_PASSWORD")
	}
	if dbname == "" {
		dbname = os.Getenv("DB_NAME")
	}
	if dbhost == "" {
		dbhost = os.Getenv("DB_HOST")
	}

	dbConnectionStr := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbhost, user, dbname, password)

	DB, err = gorm.Open("postgres", dbConnectionStr)
	if err != nil {
		panic("Error al conectar a la base de datos: " + err.Error())
	}
}
