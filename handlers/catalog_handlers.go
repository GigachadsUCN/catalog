package handlers

import (
	"catalog/models"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/mitchellh/mapstructure"
)

func AddProduct(request map[string]interface{}) ([]byte, error) {
	// Obtener los datos del producto de la solicitud
	var newProduct models.Product
	if data, ok := request["data"].(map[string]interface{}); ok {
		if err := mapstructure.Decode(data, &newProduct); err != nil {
			return nil, errors.New("Error en los datos del producto")
		}
	} else {
		return nil, errors.New("Datos del producto no encontrados en la solicitud")
	}

	if err := models.DB.Create(&newProduct).Error; err != nil {
		return nil, errors.New("Error al agregar el producto")
	}

	result, err := json.Marshal(newProduct)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetProduct(id string) ([]byte, error) {
	var product models.Product

	if err := models.DB.First(&product, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.New("Producto no encontrado")
		} else {
			return nil, err
		}
	}

	result, err := json.Marshal(product)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func HandleProductRequest(request map[string]interface{}) ([]byte, error) {
	var result interface{}

	switch pattern := request["pattern"].(string); pattern {
	case "ADD_PRODUCT":
		result, _ = AddProduct(request)
	case "DELETE_PRODUCT":
		result, _ = DeleteProduct(request)
	default:
		return nil, errors.New("Operación no soportada")
	}

	return result.([]byte), nil
}

func UpdateProduct(c *gin.Context) {
	var updateProduct models.Product
	id := c.Param("id")

	if err := c.ShouldBindJSON(&updateProduct); err != nil {
		c.JSON(http.StatusBadGateway, gin.H{"error": err.Error()})
		return
	}

	var existingProduct models.Product
	if err := models.DB.First(&existingProduct, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			c.JSON(http.StatusNotFound, gin.H{"error": "Producto no encontrado"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Error al obtener el producto"})
		}
		return
	}
	models.DB.Model(&existingProduct).Update(updateProduct)
	c.JSON(http.StatusOK, gin.H{"message": "Producto actualizado con exito"})
}

func DeleteProduct(request map[string]interface{}) ([]byte, error) {
	// Obtener el ID del producto de la solicitud
	if productID, ok := request["data"].(map[string]interface{})["id"].(string); ok {
		var existingProduct models.Product
		if err := models.DB.First(&existingProduct, productID).Error; err != nil {
			if gorm.IsRecordNotFoundError(err) {
				return nil, errors.New("Producto no encontrado")
			} else {
				return nil, err
			}
		}

		models.DB.Delete(&existingProduct)
		result, err := json.Marshal(existingProduct)
		if err != nil {
			return nil, err
		}

		return result, nil
	} else {
		return nil, errors.New("ID del producto no encontrado en la solicitud")
	}
}
func ListProducts() ([]byte, error) {
	var products []models.Product

	if err := models.DB.Find(&products).Error; err != nil {
		return nil, err
	}

	result, err := json.Marshal(gin.H{"products": products})
	if err != nil {
		return nil, err
	}

	return result, nil
}
