package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

var catalogo = map[string]interface{}{
	"products": []map[string]interface{}{
		{
			"id":          "1",
			"name":        "Producto 1",
			"stock":       10,
			"price":       20.99,
			"category":    "Electrónicos",
			"color":       "Negro",
			"genre":       true,
			"description": "Descripción del producto 1",
			"image":       "imagen1.jpg",
		},
		{
			"id":          "2",
			"name":        "Producto 2",
			"stock":       5,
			"price":       15.99,
			"category":    "Ropa",
			"color":       "Rojo",
			"genre":       false,
			"description": "Descripción del producto 2",
			"image":       "imagen2.jpg",
		},
		// Agrega más productos según sea necesario
	},
}

func getProduct(productID string) map[string]interface{} {
	for _, product := range catalogo["products"].([]map[string]interface{}) {
		if product["id"].(string) == productID {
			return product
		}
	}
	return nil
}

func adsfasdf() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"catalog", // nombre de la cola
		true,      // durable
		false,     // eliminar cuando no esté en uso
		false,     // exclusivo
		false,     // no espera a que la cola se declare
		nil,       // argumentos adicionales
	)
	if err != nil {
		log.Fatal(err)
	}

	msgs, err := ch.Consume(
		q.Name, // cola
		"",     // consumidor
		true,   // auto-ack
		false,  // exclusivo
		false,  // no local
		false,  // no espera a que el consumidor esté listo
		nil,    // argumentos adicionales
	)
	if err != nil {
		log.Fatal(err)
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			// Muestra el cuerpo del mensaje
			fmt.Printf("Body: %s\n", d.Body)

			// Decodificar el cuerpo del mensaje
			var request map[string]interface{}
			if err := json.Unmarshal(d.Body, &request); err != nil {
				log.Println("Error al decodificar el cuerpo del mensaje:", err)
				continue
			}

			// Simular algún procesamiento basado en el patrón
			var result interface{}
			switch pattern := request["pattern"].(string); pattern {
			case "GET_PRODUCT":
				productID := request["data"].(string)
				result = getProduct(productID)
			case "GET_CATALOG":
				result = catalogo
			default:
				result = "Operación no soportada"
			}

			// Estructura de la respuesta
			responseData := result

			// Codificación del cuerpo de la respuesta
			responseBody, err := json.Marshal(responseData)
			if err != nil {
				log.Println("Error al codificar la respuesta:", err)
				continue
			}

			// Publicar la respuesta en la cola de respuesta
			err = ch.Publish(
				"",        // intercambio
				d.ReplyTo, // cola de respuesta
				false,     // mandar sin confirmación
				false,     // no mandar como mandato
				amqp.Publishing{
					ContentType:   "application/json",
					CorrelationId: d.CorrelationId,
					Body:          responseBody,
				})
			if err != nil {
				log.Println("Error al publicar la respuesta:", err)
			}

			// Acknowledge del mensaje
			// d.Ack(false)
		}
	}()

	fmt.Println(" [x] Esperando solicitudes RPC. Para salir, presiona CTRL+C")
	<-forever
}
